# README #

For given book, author script will find other most correlated books within readers, which have read given book.

### Run program from command line ###

```
python3 app.py --book "the fellowship of the ring (the lord of the rings, part 1)" --author "tolkien"
```

Necessary modules are in `requirements.txt`. Tested with `Python 3.8.0 64bit`.