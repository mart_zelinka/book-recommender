import numpy as np
import pandas as pd
import argparse
import sys
from typing import Tuple

import bookRecommender as br

RATINGS_PATH = 'data/BX-Book-Ratings.csv'
BOOKS_PATH = 'data/BX-Books.csv'

def check_arg(args=None):
    parser = argparse.ArgumentParser(description='Script finds most correlated book in rating for given book, author - received as params')

    parser.add_argument('--book',
                        default=None,
                        help='book for which script finds other correlated books')
    
    parser.add_argument('--author',
                        default=None,
                        help='author of book given in --book param')

    results = parser.parse_args(args)
    return (results.book, results.author)

if __name__ == "__main__":

    book, author = check_arg(sys.argv[1:])

    ratings_Df, books_Df = br.read_csv_files(RATINGS_PATH, BOOKS_PATH)

    if not br.exist_book_in_Df(books_Df, book, author):
        print("Given book and author doesn't exists in out dataframe")
        exit(0)
    else:
        print("Computing correlated books for\n book: {}\n of author: {}\n".format(book, author))



    dataset = pd.merge(ratings_Df, books_Df, on=['ISBN'])

    rating_Df = dataset.pipe(br.filter_readers_of, book=book, author=author)\
                       .pipe(br.filter_by_number_of_ratings, numberOfratings=8)

    dataset_for_corr = rating_Df.pivot(index='User-ID', columns='Book-Title', values='Book-Rating')

    ret = br.compute_correlated_books_for(book, dataset_for_corr, rating_Df)

    print(ret)