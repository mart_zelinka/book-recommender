import pytest
import pandas as pd
from pandas._testing import assert_frame_equal

import bookRecommender as br

def test_lowercase():
    #given
    input_df  = pd.DataFrame({'col_A': ["Text", "neXT", "LAST ROW"], 'col_B': [1,2,3]})
    correct_output_df = pd.DataFrame({'col_A': ["text", "next", "last row"], 'col_B': [1,2,3]})

    #when
    output_df = br.lowercase_str_columns(input_df)

    #then
    assert_frame_equal(output_df, correct_output_df)


def test_book_author_filter():
    #given
    input_df = pd.read_csv("tests/book_author_filter.csv")
    correct_output_df = pd.read_csv("tests/book_author_filter_after.csv")

    #when
    output_df = br.filter_readers_of(input_df, "waiting to exhale", "terry mcmillan")

    #then
    assert_frame_equal(output_df.reset_index(drop=True), correct_output_df.reset_index(drop=True))


def test_num_of_ratings_filter():
    #given
    input_data = [
        [11690, 10, "painted house", "john grisham"],
        [11633, 1, "painted house", "john grisham"],
        [11676, 8,"waiting to exhale", "terry mcmillan"],
        [4234, 8,"birdsong: a novel of love and war", "sebastian faulks"],
        [11676, 8,"birdsong: a novel of love and war", "sebastian faulks"],
        [226965,5,"birdsong: a novel of love and war", "sebastian faulks"],
    ]

    output_data = [
        [4234, "birdsong: a novel of love and war", 8],
        [11676, "birdsong: a novel of love and war", 8],
        [226965, "birdsong: a novel of love and war", 5],
    ]

    input_df = pd.DataFrame(input_data, columns=['User-ID', 'Book-Rating', 'Book-Title', 'Book-Author'])
    correct_output_df = pd.DataFrame(output_data, columns=['User-ID', 'Book-Title', 'Book-Rating'])

    #when
    output_df = br.filter_by_number_of_ratings(input_df, numberOfratings=3)

    #then
    assert_frame_equal(output_df.reset_index(drop=True), correct_output_df.reset_index(drop=True))