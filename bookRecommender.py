import pandas as pd
import numpy as np
from typing import Tuple

def exist_book_in_Df(bookDf: pd.DataFrame, book, author):
    return (len(bookDf.query('`Book-Title`==@book & `Book-Author`.str.contains(@author)')) > 0)

### Functions for dataframe transformation ###

def lowercase_str_columns(df: pd.DataFrame) -> pd.DataFrame:
    stringColumns     = list(df.dtypes[df.dtypes == "object"].index)
    dataset_lowercase = df.apply(lambda x: x.str.lower().str.strip() if x.name in stringColumns else x)
    return dataset_lowercase

def read_csv_files(rating_path: str, books_path: str) -> Tuple[pd.DataFrame, pd.DataFrame]:
    # load ratings
    ratings = pd.read_csv(rating_path, encoding='cp1251', sep=';', error_bad_lines=False, warn_bad_lines=False, low_memory=False)
    ratings = ratings[ratings['Book-Rating']!=0].pipe(lowercase_str_columns)
    

    # load books
    books = (pd.read_csv('data/BX-Books.csv',  encoding='cp1251',
                         usecols=['ISBN', 'Book-Title', 'Book-Author'],
                         sep=';',
                         error_bad_lines=False)
                .dropna()
                .pipe(lowercase_str_columns))
    
    return ratings, books


def filter_readers_of(df:pd.DataFrame, book, author) -> pd.DataFrame:
    
    readers = df['User-ID'][(df['Book-Title']==book) & (df['Book-Author'].str.contains(author))]
    readers = readers.tolist()
    readers = np.unique(readers)

    # final dataset
    books_of_input_readers = df[(df['User-ID'].isin(readers))]
    return books_of_input_readers

def filter_by_number_of_ratings(df:pd.DataFrame, numberOfratings) -> pd.DataFrame:

    # Number of ratings per other books in dataset
    number_of_rating_per_book = df.groupby(['Book-Title']).agg('count').reset_index()

    #select only books which have actually higher number of ratings than threshold
    books_to_compare = number_of_rating_per_book['Book-Title'][number_of_rating_per_book['User-ID'] >= numberOfratings]
    books_to_compare = books_to_compare.tolist()

    ratings_data_raw = df[['User-ID', 'Book-Rating', 'Book-Title']][df['Book-Title'].isin(books_to_compare)]

    # group by User and Book and compute mean to remove duplicates in Book-Title col
    ratings_data_raw_nodup = ratings_data_raw.groupby(['User-ID', 'Book-Title'])['Book-Rating'].mean()
    
    #change series to dataframe
    return ratings_data_raw_nodup.to_frame().reset_index()

def compute_correlated_books_for(book: str, dataset_for_corr: pd.DataFrame, rating_df: pd.DataFrame) -> pd.DataFrame:
    
    dataset_of_other_books = dataset_for_corr.copy(deep=False)
    dataset_of_other_books.drop([book], axis=1, inplace=True)
      
    # empty lists
    book_titles = []
    correlations = []
    avgrating = []

    # corr computation
    for book_title in list(dataset_of_other_books.columns.values):
        book_titles.append(book_title)
        correlations.append(dataset_for_corr[book].corr(dataset_of_other_books[book_title]))
        rating=rating_df[rating_df['Book-Title']==book_title].groupby(rating_df['Book-Title'])["Book-Rating"].mean().iloc[0]
        avgrating.append(rating)
        
    # final dataframe of all correlation of each book   
    corr_df = pd.DataFrame(list(zip(book_titles, correlations, avgrating)), columns=['book','corr','avg_rating'])
    corr_df.head()

    # top 10 books with highest corr
    result_df = corr_df.sort_values('corr', ascending = False).head(10).reset_index(drop=True)
    
    print("List of most correlated books:")
    
    return result_df